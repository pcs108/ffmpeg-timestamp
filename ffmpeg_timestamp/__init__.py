import typing
from dataclasses import dataclass

SECONDS_IN_HOUR = 3600
SECONDS_IN_MINUTE = 60


@dataclass(slots=True)
class TimeStampValue:
    class InvalidValueError(Exception):
        pass

    value: str

    def __post_init__(self) -> None:
        if self.value.startswith("-"):
            raise TimeStampValue.InvalidValueError(
                f"Cannot have negative timestamp: {self.value}"
            )

        try:
            hours, minutes, seconds = self.value.split(":")
            float(hours)
            float(minutes)
            float(seconds)

            if len(hours) != 2 or len(minutes) != 2:
                raise ValueError(self.value)

            # Optional milliseconds as part of seconds value.
            seconds, *_ = seconds.split(".")
            if len(seconds) != 2:
                raise ValueError(self.value)
        except Exception:
            raise TimeStampValue.InvalidValueError(self.value)

    def __float__(self) -> float:
        return SecondsFromTimeStampValueParser(self).parse()

    def __int__(self) -> int:
        return int(float(self))

    def __str__(self) -> str:
        return self.value


@dataclass(slots=True)
class SecondsFromTimeStampValueParser:
    value: TimeStampValue

    def parse(self) -> float:
        hours, minutes, seconds = str(self.value).split(":")
        return (
            (int(hours) * SECONDS_IN_HOUR)
            + (int(minutes) * SECONDS_IN_MINUTE)
            + float(seconds)
        )


@dataclass(slots=True)
class HHMMSSToTimeStampValueParser:
    EMPTY_VALUE = "00:00:00"
    FULL_STAMP_SEPARATORS = 2

    raw_value: str

    @property
    def number_of_separators(self) -> int:
        return self.raw_value.count(":")

    def parse(self) -> TimeStampValue:
        if not self.raw_value:
            return TimeStampValue(HHMMSSToTimeStampValueParser.EMPTY_VALUE)

        number_of_empty_separators = (
            HHMMSSToTimeStampValueParser.FULL_STAMP_SEPARATORS
            - self.number_of_separators
        )
        padding = ["00"] * number_of_empty_separators
        return TimeStampValue(":".join(padding + [self.raw_value]))


@dataclass(slots=True)
class NumberOfSecondsToTimeStampValueParser:
    class InvalidValueError(Exception):
        pass

    ACCEPTED_TYPES = [str, int, float]

    raw_value: typing.Union[str, int, float]

    def __post_init__(self):
        try:
            if (
                type(self.raw_value)
                not in NumberOfSecondsToTimeStampValueParser.ACCEPTED_TYPES
            ):
                raise TypeError(self.raw_value)
            as_number = float(self.raw_value)
            if as_number < 0:
                raise ValueError(self.raw_value)
        except Exception:
            raise NumberOfSecondsToTimeStampValueParser.InvalidValueError(
                self.raw_value
            )

    def parse(self) -> TimeStampValue:
        value = str(self.raw_value)
        seconds, *milliseconds = value.split(".")
        if milliseconds:
            milliseconds = f".{milliseconds[0]}"
        else:
            milliseconds = ""

        seconds = int(seconds)

        number_of_hours = int(seconds // SECONDS_IN_HOUR)
        hours = str(number_of_hours).zfill(2)
        seconds -= number_of_hours * SECONDS_IN_HOUR

        number_of_minutes = int(seconds // SECONDS_IN_MINUTE)
        minutes = str(number_of_minutes).zfill(2)
        seconds -= number_of_minutes * SECONDS_IN_MINUTE

        seconds = str(seconds).zfill(2)

        parsed = f"{hours}:{minutes}:{seconds}{milliseconds}"
        return TimeStampValue(parsed)


@dataclass(slots=True)
class TimeStamp:
    value: TimeStampValue

    def __eq__(self, other: typing.Any) -> bool:
        if isinstance(other, TimeStamp):
            return self.value == other.value
        else:
            try:
                return self.value == TimeStamp.from_value(other).value
            except Exception:
                return False

    def __add__(self, other: typing.Union["TimeStamp", str, int, float]) -> "TimeStamp":
        if not isinstance(other, TimeStamp):
            other = TimeStamp.from_value(other)
        return TimeStampAdder(self, other).add()

    def __sub__(self, other: typing.Union["TimeStamp", str, int, float]) -> "TimeStamp":
        if not isinstance(other, TimeStamp):
            other = TimeStamp.from_value(other)
        return TimeStampSubtracter(self, other).subtract()

    def __truediv__(
        self, other: typing.Union["TimeStamp", str, int, float]
    ) -> typing.List[typing.Tuple["TimeStamp"]]:
        if not isinstance(other, TimeStamp):
            other = TimeStamp.from_value(other)
        return TimeStampDivider(self, other).divide()

    def __floordiv__(
        self, other: typing.Union["TimeStamp", str, int, float]
    ) -> typing.List[typing.Tuple["TimeStamp"]]:
        if not isinstance(other, TimeStamp):
            if isinstance(other, float):
                other = int(other)
            other = TimeStamp.from_value(other)
        return TimeStampDivider(self, other).divide()

    def __float__(self) -> float:
        return float(self.value)

    def __int__(self) -> int:
        return int(self.value)

    def __str__(self) -> str:
        return str(self.value)

    @classmethod
    def from_value(cls, value: typing.Union[str, int, float]) -> "TimeStamp":
        if isinstance(value, str) and ":" in value:
            time_stamp_value = HHMMSSToTimeStampValueParser(value).parse()
        else:
            time_stamp_value = NumberOfSecondsToTimeStampValueParser(value).parse()
        return TimeStamp(time_stamp_value)


@dataclass(slots=True)
class TimeStampAdder:
    value: TimeStamp
    other: TimeStamp

    def add(self) -> TimeStamp:
        seconds_in_value = SecondsFromTimeStampValueParser(self.value.value).parse()
        seconds_in_other = SecondsFromTimeStampValueParser(self.other.value).parse()
        result = seconds_in_value + seconds_in_other
        return TimeStamp.from_value(result)


@dataclass(slots=True)
class TimeStampDivider:
    value: TimeStamp
    other: TimeStamp

    def divide(self) -> typing.List[typing.Tuple[TimeStamp]]:
        value_in_seconds = float(self.value)
        other_in_seconds = float(self.other)
        number_of_values = int(value_in_seconds // other_in_seconds)

        remainder = value_in_seconds

        results = []
        for num in range(number_of_values + 1):
            if remainder <= 0:
                break

            start = num * other_in_seconds
            if remainder > other_in_seconds:
                end = (num + 1) * other_in_seconds
            else:
                end = start + remainder
            results.append((TimeStamp.from_value(start), TimeStamp.from_value(end)))
            remainder -= other_in_seconds
        return results


@dataclass(slots=True)
class TimeStampSubtracter:
    value: TimeStamp
    other: TimeStamp

    def subtract(self) -> TimeStamp:
        seconds_in_value = SecondsFromTimeStampValueParser(self.value.value).parse()
        seconds_in_other = SecondsFromTimeStampValueParser(self.other.value).parse()
        result = seconds_in_value - seconds_in_other
        return TimeStamp.from_value(result)
