from ffmpeg_timestamp import TimeStamp, TimeStampSubtracter, TimeStampValue


def test_time_stamp_subtracter_subtracts_seconds_to_seconds():
    value = TimeStamp(TimeStampValue("00:00:02"))
    other = TimeStamp(TimeStampValue("00:00:01"))
    assert TimeStampSubtracter(value, other).subtract() == TimeStamp(
        TimeStampValue("00:00:01.0")
    )


def test_time_stamp_subtracter_subtracts_seconds_to_minutes():
    value = TimeStamp(TimeStampValue("00:02:00"))
    other = TimeStamp(TimeStampValue("00:00:02"))
    assert TimeStampSubtracter(value, other).subtract() == TimeStamp(
        TimeStampValue("00:01:58.0")
    )


def test_time_stamp_subtracter_subtracts_minutes_to_minutes():
    value = TimeStamp(TimeStampValue("00:03:00"))
    other = TimeStamp(TimeStampValue("00:02:00"))
    assert TimeStampSubtracter(value, other).subtract() == TimeStamp(
        TimeStampValue("00:01:00.0")
    )


def test_time_stamp_subtracter_subtracts_mixed_minutes_and_seconds():
    value = TimeStamp(TimeStampValue("00:32:00"))
    other = TimeStamp(TimeStampValue("00:10:03"))
    assert TimeStampSubtracter(value, other).subtract() == TimeStamp(
        TimeStampValue("00:21:57.0")
    )


def test_time_stamp_subtracter_subtracts_seconds_to_hours():
    value = TimeStamp(TimeStampValue("02:00:00"))
    other = TimeStamp(TimeStampValue("00:00:29"))
    assert TimeStampSubtracter(value, other).subtract() == TimeStamp(
        TimeStampValue("01:59:31.0")
    )


def test_time_stamp_subtracter_subtracts_minutes_to_hours():
    value = TimeStamp(TimeStampValue("02:00:00"))
    other = TimeStamp(TimeStampValue("00:31:00"))
    assert TimeStampSubtracter(value, other).subtract() == TimeStamp(
        TimeStampValue("01:29:00.0")
    )


def test_time_stamp_subtracter_subtracts_mixed_minutes_and_seconds_to_hours():
    value = TimeStamp(TimeStampValue("02:00:00"))
    other = TimeStamp(TimeStampValue("00:31:15"))
    assert TimeStampSubtracter(value, other).subtract() == TimeStamp(
        TimeStampValue("01:28:45.0")
    )


def test_time_stamp_subtracter_subtracts_hours_to_hours():
    value = TimeStamp(TimeStampValue("02:00:00"))
    other = TimeStamp(TimeStampValue("01:00:00"))
    assert TimeStampSubtracter(value, other).subtract() == TimeStamp(
        TimeStampValue("01:00:00.0")
    )


def test_time_stamp_subtracter_subtracts_mixed_hours_and_minutes_to_hours():
    value = TimeStamp(TimeStampValue("02:00:00"))
    other = TimeStamp(TimeStampValue("01:44:00"))
    assert TimeStampSubtracter(value, other).subtract() == TimeStamp(
        TimeStampValue("00:16:00.0")
    )


def test_time_stamp_subtracter_subtracts_mixed_hours_and_seconds_to_hours():
    value = TimeStamp(TimeStampValue("02:00:00"))
    other = TimeStamp(TimeStampValue("01:00:44"))
    assert TimeStampSubtracter(value, other).subtract() == TimeStamp(
        TimeStampValue("00:59:16.0")
    )


def test_time_stamp_subtracter_subtracts_mixed_hours_and_minutes_and_seconds_to_hours():
    value = TimeStamp(TimeStampValue("02:00:00"))
    other = TimeStamp(TimeStampValue("01:23:44"))
    assert TimeStampSubtracter(value, other).subtract() == TimeStamp(
        TimeStampValue("00:36:16.0")
    )
