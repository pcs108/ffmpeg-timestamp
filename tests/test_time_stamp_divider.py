from ffmpeg_timestamp import (
    NumberOfSecondsToTimeStampValueParser,
    TimeStamp,
    TimeStampDivider,
)


def test_time_stamp_value_divider_divides_seconds_by_seconds():
    value = TimeStamp.from_value("00:00:06")
    other = TimeStamp.from_value("00:00:02")
    assert TimeStampDivider(value, other).divide() == [
        (TimeStamp.from_value("00:00:00.0"), TimeStamp.from_value("00:00:02.0")),
        (TimeStamp.from_value("00:00:02.0"), TimeStamp.from_value("00:00:04.0")),
        (TimeStamp.from_value("00:00:04.0"), TimeStamp.from_value("00:00:06.0")),
    ]


def test_time_stamp_value_divider_divides_seconds_by_seconds_and_handles_overflow():
    value = TimeStamp.from_value("00:00:07")
    other = TimeStamp.from_value("00:00:02")
    assert TimeStampDivider(value, other).divide() == [
        (TimeStamp.from_value("00:00:00.0"), TimeStamp.from_value("00:00:02.0")),
        (TimeStamp.from_value("00:00:02.0"), TimeStamp.from_value("00:00:04.0")),
        (TimeStamp.from_value("00:00:04.0"), TimeStamp.from_value("00:00:06.0")),
        (TimeStamp.from_value("00:00:06.0"), TimeStamp.from_value("00:00:07.0")),
    ]


def test_time_stamp_value_divider_divides_minutes_by_seconds():
    value = TimeStamp.from_value("00:02:00")
    other = TimeStamp.from_value("00:00:30")
    assert TimeStampDivider(value, other).divide() == [
        (TimeStamp.from_value("00:00:00.0"), TimeStamp.from_value("00:00:30.0")),
        (TimeStamp.from_value("00:00:30.0"), TimeStamp.from_value("00:01:00.0")),
        (TimeStamp.from_value("00:01:00.0"), TimeStamp.from_value("00:01:30.0")),
        (TimeStamp.from_value("00:01:30.0"), TimeStamp.from_value("00:02:00.0")),
    ]


def test_time_stamp_value_divider_divides_minutes_by_seconds_and_handles_overflow():
    value = TimeStamp.from_value("00:02:15")
    other = TimeStamp.from_value("00:00:30")
    assert TimeStampDivider(value, other).divide() == [
        (TimeStamp.from_value("00:00:00.0"), TimeStamp.from_value("00:00:30.0")),
        (TimeStamp.from_value("00:00:30.0"), TimeStamp.from_value("00:01:00.0")),
        (TimeStamp.from_value("00:01:00.0"), TimeStamp.from_value("00:01:30.0")),
        (TimeStamp.from_value("00:01:30.0"), TimeStamp.from_value("00:02:00.0")),
        (TimeStamp.from_value("00:02:00.0"), TimeStamp.from_value("00:02:15.0")),
    ]


def test_time_stamp_value_divider_divides_minutes_by_minutes():
    value = TimeStamp.from_value("00:06:00")
    other = TimeStamp.from_value("00:02:00")
    assert TimeStampDivider(value, other).divide() == [
        (TimeStamp.from_value("00:00:00.0"), TimeStamp.from_value("00:02:00.0")),
        (TimeStamp.from_value("00:02:00.0"), TimeStamp.from_value("00:04:00.0")),
        (TimeStamp.from_value("00:04:00.0"), TimeStamp.from_value("00:06:00.0")),
    ]


def test_time_stamp_value_divider_divides_minutes_by_minutes_and_handles_overflow():
    value = TimeStamp.from_value("00:06:23")
    other = TimeStamp.from_value("00:02:00")
    assert TimeStampDivider(value, other).divide() == [
        (TimeStamp.from_value("00:00:00.0"), TimeStamp.from_value("00:02:00.0")),
        (TimeStamp.from_value("00:02:00.0"), TimeStamp.from_value("00:04:00.0")),
        (TimeStamp.from_value("00:04:00.0"), TimeStamp.from_value("00:06:00.0")),
        (TimeStamp.from_value("00:06:00.0"), TimeStamp.from_value("00:06:23.0")),
    ]


def test_time_stamp_value_divider_divides_minutes_by_mixed_seconds_and_minutes():
    value = TimeStamp.from_value("00:04:09")
    other = TimeStamp.from_value("00:01:23")
    assert TimeStampDivider(value, other).divide() == [
        (TimeStamp.from_value("00:00:00.0"), TimeStamp.from_value("00:01:23.0")),
        (TimeStamp.from_value("00:01:23.0"), TimeStamp.from_value("00:02:46.0")),
        (TimeStamp.from_value("00:02:46.0"), TimeStamp.from_value("00:04:09.0")),
    ]


def test_time_stamp_value_divider_divides_minutes_by_mixed_seconds_handles_overflow():
    value = TimeStamp.from_value("00:04:11")
    other = TimeStamp.from_value("00:01:23")
    assert TimeStampDivider(value, other).divide() == [
        (TimeStamp.from_value("00:00:00.0"), TimeStamp.from_value("00:01:23.0")),
        (TimeStamp.from_value("00:01:23.0"), TimeStamp.from_value("00:02:46.0")),
        (TimeStamp.from_value("00:02:46.0"), TimeStamp.from_value("00:04:09.0")),
        (TimeStamp.from_value("00:04:09.0"), TimeStamp.from_value("00:04:11.0")),
    ]


def test_time_stamp_value_divider_divides_hours_by_seconds():
    value = TimeStamp.from_value("01:00:00")
    other = TimeStamp.from_value("00:00:01")
    assert TimeStampDivider(value, other).divide() == [
        (
            TimeStamp(NumberOfSecondsToTimeStampValueParser(float(i)).parse()),
            TimeStamp(NumberOfSecondsToTimeStampValueParser(float(i + 1)).parse()),
        )
        for i in range(3600)
    ]


def test_time_stamp_value_divider_divides_hours_by_seconds_and_handles_overflow():
    value = TimeStamp.from_value("01:00:01")
    other = TimeStamp.from_value("00:00:01")
    assert TimeStampDivider(value, other).divide() == [
        (
            TimeStamp(NumberOfSecondsToTimeStampValueParser(float(i)).parse()),
            TimeStamp(NumberOfSecondsToTimeStampValueParser(float(i + 1)).parse()),
        )
        for i in range(3600)
    ] + [(TimeStamp.from_value("01:00:00.0"), TimeStamp.from_value("01:00:01.0"))]


def test_time_stamp_value_divider_divides_hours_by_minutes():
    value = TimeStamp.from_value("02:00:00")
    other = TimeStamp.from_value("00:30:00")
    assert TimeStampDivider(value, other).divide() == [
        (TimeStamp.from_value("00:00:00.0"), TimeStamp.from_value("00:30:00.0")),
        (TimeStamp.from_value("00:30:00.0"), TimeStamp.from_value("01:00:00.0")),
        (TimeStamp.from_value("01:00:00.0"), TimeStamp.from_value("01:30:00.0")),
        (TimeStamp.from_value("01:30:00.0"), TimeStamp.from_value("02:00:00.0")),
    ]


def test_time_stamp_value_divider_divides_hours_by_minutes_and_handles_overflow():
    value = TimeStamp.from_value("02:02:00")
    other = TimeStamp.from_value("00:30:00")
    assert TimeStampDivider(value, other).divide() == [
        (TimeStamp.from_value("00:00:00.0"), TimeStamp.from_value("00:30:00.0")),
        (TimeStamp.from_value("00:30:00.0"), TimeStamp.from_value("01:00:00.0")),
        (TimeStamp.from_value("01:00:00.0"), TimeStamp.from_value("01:30:00.0")),
        (TimeStamp.from_value("01:30:00.0"), TimeStamp.from_value("02:00:00.0")),
        (TimeStamp.from_value("02:00:00.0"), TimeStamp.from_value("02:02:00.0")),
    ]


def test_time_stamp_value_divider_divides_hours_by_hours():
    value = TimeStamp.from_value("04:00:00")
    other = TimeStamp.from_value("02:00:00")
    assert TimeStampDivider(value, other).divide() == [
        (TimeStamp.from_value("00:00:00.0"), TimeStamp.from_value("02:00:00.0")),
        (TimeStamp.from_value("02:00:00.0"), TimeStamp.from_value("04:00:00.0")),
    ]


def test_time_stamp_value_divider_divides_hours_by_hours_and_handles_overflow():
    value = TimeStamp.from_value("04:32:10")
    other = TimeStamp.from_value("02:00:00")
    assert TimeStampDivider(value, other).divide() == [
        (TimeStamp.from_value("00:00:00.0"), TimeStamp.from_value("02:00:00.0")),
        (TimeStamp.from_value("02:00:00.0"), TimeStamp.from_value("04:00:00.0")),
        (TimeStamp.from_value("04:00:00.0"), TimeStamp.from_value("04:32:10.0")),
    ]


def test_time_stamp_value_divider_divides_hours_by_mixed_minutes_and_seconds():
    value = TimeStamp.from_value("01:02:50")
    other = TimeStamp.from_value("00:12:34")
    assert TimeStampDivider(value, other).divide() == [
        (TimeStamp.from_value("00:00:00.0"), TimeStamp.from_value("00:12:34.0")),
        (TimeStamp.from_value("00:12:34.0"), TimeStamp.from_value("00:25:08.0")),
        (TimeStamp.from_value("00:25:08.0"), TimeStamp.from_value("00:37:42.0")),
        (TimeStamp.from_value("00:37:42.0"), TimeStamp.from_value("00:50:16.0")),
        (TimeStamp.from_value("00:50:16.0"), TimeStamp.from_value("01:02:50.0")),
    ]


def test_time_stamp_value_divider_divides_hours_by_mixed_mins_secs_with_overflow():
    value = TimeStamp.from_value("01:02:53")
    other = TimeStamp.from_value("00:12:34")
    assert TimeStampDivider(value, other).divide() == [
        (TimeStamp.from_value("00:00:00.0"), TimeStamp.from_value("00:12:34.0")),
        (TimeStamp.from_value("00:12:34.0"), TimeStamp.from_value("00:25:08.0")),
        (TimeStamp.from_value("00:25:08.0"), TimeStamp.from_value("00:37:42.0")),
        (TimeStamp.from_value("00:37:42.0"), TimeStamp.from_value("00:50:16.0")),
        (TimeStamp.from_value("00:50:16.0"), TimeStamp.from_value("01:02:50.0")),
        (TimeStamp.from_value("01:02:50.0"), TimeStamp.from_value("01:02:53.0")),
    ]


def test_time_stamp_value_divider_divides_hours_by_mixed_hours_mins_secs():
    value = TimeStamp.from_value("04:11:15")
    other = TimeStamp.from_value("01:23:45")
    result = TimeStampDivider(value, other).divide()
    assert result == [
        (TimeStamp.from_value("00:00:00.0"), TimeStamp.from_value("01:23:45.0")),
        (TimeStamp.from_value("01:23:45.0"), TimeStamp.from_value("02:47:30.0")),
        (TimeStamp.from_value("02:47:30.0"), TimeStamp.from_value("04:11:15.0")),
    ]


def test_time_stamp_value_divider_divides_hours_by_mixed_hours_mins_secs_overflow():
    value = TimeStamp.from_value("04:12:17")
    other = TimeStamp.from_value("01:23:45")
    result = TimeStampDivider(value, other).divide()
    assert result == [
        (TimeStamp.from_value("00:00:00.0"), TimeStamp.from_value("01:23:45.0")),
        (TimeStamp.from_value("01:23:45.0"), TimeStamp.from_value("02:47:30.0")),
        (TimeStamp.from_value("02:47:30.0"), TimeStamp.from_value("04:11:15.0")),
        (TimeStamp.from_value("04:11:15.0"), TimeStamp.from_value("04:12:17.0")),
    ]
