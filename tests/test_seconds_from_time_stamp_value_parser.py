from ffmpeg_timestamp import SecondsFromTimeStampValueParser, TimeStampValue


def test_seconds_parser_parses_seconds():
    assert SecondsFromTimeStampValueParser(TimeStampValue("00:00:02")).parse() == 2


def test_seconds_parser_parses_seconds_with_milliseconds():
    assert (
        SecondsFromTimeStampValueParser(TimeStampValue("00:00:02.34")).parse() == 2.34
    )


def test_seconds_parser_parses_minutes():
    assert SecondsFromTimeStampValueParser(TimeStampValue("00:12:00")).parse() == 720


def test_seconds_parser_parses_minutes_with_milliseconds():
    assert (
        SecondsFromTimeStampValueParser(TimeStampValue("00:12:00.345")).parse()
        == 720.345
    )


def test_seconds_parser_parses_minutes_and_seconds():
    assert SecondsFromTimeStampValueParser(TimeStampValue("00:12:34")).parse() == 754


def test_seconds_parser_parses_minutes_and_seconds_with_milliseconds():
    assert (
        SecondsFromTimeStampValueParser(TimeStampValue("00:12:34.567")).parse()
        == 754.567
    )


def test_seconds_parser_parses_hours():
    assert SecondsFromTimeStampValueParser(TimeStampValue("02:00:00")).parse() == 7200


def test_seconds_parser_parses_hours_with_milliseconds():
    assert (
        SecondsFromTimeStampValueParser(TimeStampValue("02:00:00.34")).parse()
        == 7200.34
    )


def test_seconds_parser_parses_hours_and_minutes():
    assert SecondsFromTimeStampValueParser(TimeStampValue("02:34:00")).parse() == 9240


def test_seconds_parser_parses_hours_and_minutes_with_milliseconds():
    assert (
        SecondsFromTimeStampValueParser(TimeStampValue("02:34:00.56")).parse()
        == 9240.56
    )


def test_seconds_parser_parses_hours_and_minutes_and_seconds():
    assert SecondsFromTimeStampValueParser(TimeStampValue("02:34:56")).parse() == 9296


def test_seconds_parser_parses_hours_and_minutes_and_seconds_with_milliseconds():
    assert (
        SecondsFromTimeStampValueParser(TimeStampValue("02:34:56.789")).parse()
        == 9296.789
    )
