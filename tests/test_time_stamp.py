import pytest

from ffmpeg_timestamp import TimeStamp, TimeStampValue


def test_time_stamp_from_value_parses_number_of_seconds():
    assert TimeStamp.from_value("123") == TimeStamp(value=TimeStampValue("00:02:03"))


def test_time_stamp_from_value_parses_hhmmss_stamp():
    assert TimeStamp.from_value("01:23") == TimeStamp(value=TimeStampValue("00:01:23"))


@pytest.mark.parametrize("value", ["00:01:23", 123])
def test_time_stamp_is_equal_to_equivalent_values(value):
    time_stamp = TimeStamp.from_value(value)
    assert time_stamp == value


@pytest.mark.parametrize("value", ["00:01:23", 123, True])
def test_time_stamp_is_not_equal_to_unequivalent_values(value):
    time_stamp = TimeStamp.from_value("01:02:03")
    assert time_stamp != value


@pytest.mark.parametrize(
    "other", [TimeStamp(TimeStampValue("00:00:02")), 2, 2.0, "2", "00:02"]
)
def test_time_stamp_addition_works_on_all_accepted_types(other):
    time_stamp = TimeStamp(TimeStampValue("00:00:04"))
    assert time_stamp + other == TimeStamp(TimeStampValue("00:00:06.0"))


@pytest.mark.parametrize(
    "other", [TimeStamp(TimeStampValue("00:00:02")), 2, 2.0, "2", "00:02"]
)
def test_time_stamp_subtraction_works_on_all_accepted_types(other):
    time_stamp = TimeStamp(TimeStampValue("00:00:04"))
    assert time_stamp - other == TimeStamp(TimeStampValue("00:00:02.0"))


@pytest.mark.parametrize(
    "other", [TimeStamp(TimeStampValue("00:00:02")), 2, 2.0, 2.34, "2", "00:02"]
)
def test_time_stamp_floor_division_works_on_all_accepted_types(other):
    time_stamp = TimeStamp(TimeStampValue("00:00:04"))
    assert time_stamp // other == [
        (
            TimeStamp(TimeStampValue("00:00:00.0")),
            TimeStamp(TimeStampValue("00:00:02.0")),
        ),
        (
            TimeStamp(TimeStampValue("00:00:02.0")),
            TimeStamp(TimeStampValue("00:00:04.0")),
        ),
    ]


@pytest.mark.parametrize(
    "other", [TimeStamp(TimeStampValue("00:00:02")), 2, 2.0, "2", "00:02"]
)
def test_time_stamp_true_division_works_on_all_accepted_types(other):
    time_stamp = TimeStamp(TimeStampValue("00:00:04"))
    assert time_stamp / other == [
        (
            TimeStamp(TimeStampValue("00:00:00.0")),
            TimeStamp(TimeStampValue("00:00:02.0")),
        ),
        (
            TimeStamp(TimeStampValue("00:00:02.0")),
            TimeStamp(TimeStampValue("00:00:04.0")),
        ),
    ]


def test_time_stamp_casts_to_float():
    assert float(TimeStamp(TimeStampValue("01:23:34.567"))) == 5014.567


def test_time_stamp_casts_to_int():
    assert int(TimeStamp(TimeStampValue("01:23:34.567"))) == 5014


def test_time_stamp_casts_to_string():
    time_stamp = TimeStamp(TimeStampValue("00:01:23"))
    assert str(time_stamp) == "00:01:23"
