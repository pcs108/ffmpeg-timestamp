from ffmpeg_timestamp import TimeStamp, TimeStampAdder, TimeStampValue


def test_time_stamp_adder_adds_seconds_to_seconds():
    value = TimeStamp(TimeStampValue("00:00:02"))
    other = TimeStamp(TimeStampValue("00:00:01"))
    assert TimeStampAdder(value, other).add() == TimeStamp(TimeStampValue("00:00:03.0"))


def test_time_stamp_adder_adds_seconds_to_minutes():
    value = TimeStamp(TimeStampValue("00:02:00"))
    other = TimeStamp(TimeStampValue("00:00:01"))
    assert TimeStampAdder(value, other).add() == TimeStamp(TimeStampValue("00:02:01.0"))


def test_time_stamp_adder_adds_minutes_to_minutes():
    value = TimeStamp(TimeStampValue("00:02:00"))
    other = TimeStamp(TimeStampValue("00:03:00"))
    assert TimeStampAdder(value, other).add() == TimeStamp(TimeStampValue("00:05:00.0"))


def test_time_stamp_adder_adds_mixed_minutes_and_seconds():
    value = TimeStamp(TimeStampValue("00:32:00"))
    other = TimeStamp(TimeStampValue("00:10:01"))
    assert TimeStampAdder(value, other).add() == TimeStamp(TimeStampValue("00:42:01.0"))


def test_time_stamp_adder_adds_seconds_to_hours():
    value = TimeStamp(TimeStampValue("02:00:00"))
    other = TimeStamp(TimeStampValue("00:00:34"))
    assert TimeStampAdder(value, other).add() == TimeStamp(TimeStampValue("02:00:34.0"))


def test_time_stamp_adder_adds_minutes_to_hours():
    value = TimeStamp(TimeStampValue("02:00:00"))
    other = TimeStamp(TimeStampValue("00:34:00"))
    assert TimeStampAdder(value, other).add() == TimeStamp(TimeStampValue("02:34:00.0"))


def test_time_stamp_adder_adds_mixed_minutes_and_seconds_to_hours():
    value = TimeStamp(TimeStampValue("02:00:00"))
    other = TimeStamp(TimeStampValue("00:34:56"))
    assert TimeStampAdder(value, other).add() == TimeStamp(TimeStampValue("02:34:56.0"))


def test_time_stamp_adder_adds_hours_to_hours():
    value = TimeStamp(TimeStampValue("02:00:00"))
    other = TimeStamp(TimeStampValue("01:00:00"))
    assert TimeStampAdder(value, other).add() == TimeStamp(TimeStampValue("03:00:00.0"))


def test_time_stamp_adder_adds_mixed_hours_and_minutes_to_hours():
    value = TimeStamp(TimeStampValue("02:00:00"))
    other = TimeStamp(TimeStampValue("01:44:00"))
    assert TimeStampAdder(value, other).add() == TimeStamp(TimeStampValue("03:44:00.0"))


def test_time_stamp_adder_adds_mixed_hours_and_seconds_to_hours():
    value = TimeStamp(TimeStampValue("02:00:00"))
    other = TimeStamp(TimeStampValue("01:00:44"))
    assert TimeStampAdder(value, other).add() == TimeStamp(TimeStampValue("03:00:44.0"))


def test_time_stamp_adder_adds_mixed_hours_and_minutes_and_seconds_to_hours():
    value = TimeStamp(TimeStampValue("02:00:00"))
    other = TimeStamp(TimeStampValue("01:23:44"))
    assert TimeStampAdder(value, other).add() == TimeStamp(TimeStampValue("03:23:44.0"))
