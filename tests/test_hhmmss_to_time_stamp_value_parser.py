from ffmpeg_timestamp import HHMMSSToTimeStampValueParser, TimeStampValue


def test_full_stamp_is_returned():
    assert HHMMSSToTimeStampValueParser("01:02:03").parse() == TimeStampValue(
        "01:02:03"
    )


def test_full_stamp_with_milliseconds_is_returned():
    assert HHMMSSToTimeStampValueParser("01:02:03.456").parse() == TimeStampValue(
        "01:02:03.456"
    )


def test_stamp_without_hours_is_filled_in():
    assert HHMMSSToTimeStampValueParser("01:02").parse() == TimeStampValue("00:01:02")


def test_stamp_without_hours_with_milliseconds_is_filled_in():
    assert HHMMSSToTimeStampValueParser("01:02.345").parse() == TimeStampValue(
        "00:01:02.345"
    )


def test_stamp_without_hours_or_minutes_is_filled_in():
    assert HHMMSSToTimeStampValueParser("12").parse() == TimeStampValue("00:00:12")


def test_stamp_without_hours_or_minutes_with_milliseconds_is_filled_in():
    assert HHMMSSToTimeStampValueParser("12.345").parse() == TimeStampValue(
        "00:00:12.345"
    )


def test_empty_time_stamp_is_filled_in():
    assert HHMMSSToTimeStampValueParser("").parse() == TimeStampValue("00:00:00")
