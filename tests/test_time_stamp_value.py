import pytest

from ffmpeg_timestamp import TimeStampValue


def test_time_stamp_value_accepts_valid_time_stamp():
    assert str(TimeStampValue("01:02:34")) == "01:02:34"


def test_time_stamp_value_accepts_valid_time_stamp_with_milliseconds():
    assert str(TimeStampValue("01:02:34.567")) == "01:02:34.567"


@pytest.mark.parametrize(
    "value",
    [
        "AA:BB:CC",
        "-AA:BB:CC",
        "AA:BB:CC.123",
        "-AA:BB:CC.123",
        "A:B:C",
        "-A:B:C",
        "A:B:C.12",
        "-A:B:C.12",
        "1",
        "-1",
        "1.234",
        "-1.234",
        "01",
        "-01",
        "01.234",
        "-01.234",
        ":01",
        "-:01",
        ":01.234",
        "-:01.234",
        "1:2",
        "-1:2",
        "1:2.345",
        "-1:2.345",
        "1:23",
        "-1:23",
        "1:23.456",
        "-1:23.456",
        "01:23",
        "-01:23",
        "01:23.456",
        "-01:23.456",
        "1:2:3",
        "-1:2:3",
        "1:2:3.456",
        "-1:2:3.456",
        "1:2:34",
        "-1:2:34",
        "1:2:34.567",
        "-1:2:34.567",
        "1:23:4",
        "-1:23:4",
        "1:23:4.567",
        "-1:23:4.567",
        "1:23:45",
        "-1:23:45",
        "1:23:45.678",
        "-1:23:45.678",
        "-01:23:45",
        "-01:23:45.678",
    ],
)
def test_time_stamp_value_raises_error_for_invalid_time_stamps(value):
    with pytest.raises(TimeStampValue.InvalidValueError):
        TimeStampValue(value)


def test_time_stamp_value_casts_to_float():
    assert float(TimeStampValue("01:23:34.567")) == 5014.567


def test_time_stamp_value_casts_to_int():
    assert int(TimeStampValue("01:23:34.567")) == 5014


def test_time_stamp_value_casts_to_string():
    value = TimeStampValue("01:23:45")
    assert str(value) == "01:23:45"
