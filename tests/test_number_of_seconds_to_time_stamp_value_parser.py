import pytest

from ffmpeg_timestamp import NumberOfSecondsToTimeStampValueParser, TimeStampValue


def test_seconds_are_parsed():
    assert NumberOfSecondsToTimeStampValueParser(12).parse() == TimeStampValue(
        "00:00:12"
    )


def test_seconds_are_parsed_with_milliseconds():
    assert NumberOfSecondsToTimeStampValueParser(12.345).parse() == TimeStampValue(
        "00:00:12.345"
    )


def test_single_digit_seconds_are_padded():
    assert NumberOfSecondsToTimeStampValueParser(3).parse() == TimeStampValue(
        "00:00:03"
    )


def test_single_digit_seconds_with_milliseconds_are_padded():
    assert NumberOfSecondsToTimeStampValueParser(3.456).parse() == TimeStampValue(
        "00:00:03.456"
    )


def test_minutes_are_parsed():
    assert NumberOfSecondsToTimeStampValueParser(754).parse() == TimeStampValue(
        "00:12:34"
    )


def test_minutes_are_parsed_with_milliseconds():
    assert NumberOfSecondsToTimeStampValueParser(754.567).parse() == TimeStampValue(
        "00:12:34.567"
    )


def test_hours_are_parsed():
    assert NumberOfSecondsToTimeStampValueParser(45296).parse() == TimeStampValue(
        "12:34:56"
    )


def test_hours_are_parsed_with_milliseconds():
    assert NumberOfSecondsToTimeStampValueParser(45296.789).parse() == TimeStampValue(
        "12:34:56.789"
    )


@pytest.mark.parametrize("value", [-1, "A", None, "-A"])
def test_invalid_values_are_not_accepted(value):
    with pytest.raises(NumberOfSecondsToTimeStampValueParser.InvalidValueError):
        NumberOfSecondsToTimeStampValueParser(value)
